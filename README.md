# Learning ML
I started  this repo when I went through ML  learning videos.
I am starting with an example from https://www.youtube.com/watch?v=qFJeN9V1ZsI

## How to run the py files?
- `./run.sh` starts a docker container that exposes a jupyter notebook at http://localhost:8888
  - you can paste the py files in there and see the outcome
    Note: If you have no idea of using tensorflow or keras or any ML lib, all this will not make any sense,
    at least  that's how it was to me in the beginning.
- `./run.sh bash` will start the docker container with python and tensorflow installed
  and open a bash, do `cd /app` and you find all the files of this repo mapped into the container
  - `python 2-even-od-odd.py` will run the according file which trains the model and runs a prediction
  
## Run the first example
To run  the first example from the 
[TensorFlow video course](https://www.youtube.com/watch?v=qFJeN9V1ZsI)
you can do  the following:
- prerequisite is to have docker installed on your machine, since  all  of this is installed and runs inside
  a docker container, to prevent having local installs and to be able to reproduce a working
  environment
- `./run.sh python 1-patients-example1.py`

This should yield output like this (shortened):
```bash
> ./run.sh ./1-patients-example1.py 
--- BUILDING image 'jupyter:....'---
Sending build context to Docker daemon  3.072kB
Step 1/3 : FROM jupyter/datascience-notebook
...
--- RUNNING container 'jupyter'---
...
Epoch 1/30
189/189 - 1s - loss: 0.6621 - accuracy: 0.5868 - val_loss: 0.6418 - val_accuracy: 0.6714
Epoch 2/30
189/189 - 0s - loss: 0.6360 - accuracy: 0.6487 - val_loss: 0.6118 - val_accuracy: 0.7286
Epoch 3/30
189/189 - 0s - loss: 0.6085 - accuracy: 0.6937 - val_loss: 0.5818 - val_accuracy: 0.7429
Epoch 4/30
189/189 - 0s - loss: 0.5787 - accuracy: 0.7524 - val_loss: 0.5496 - val_accuracy: 0.7810
Epoch 5/30
...
Epoch 30/30
189/189 - 0s - loss: 0.2781 - accuracy: 0.9275 - val_loss: 0.2680 - val_accuracy: 0.9190
[[0.9476151  0.05238493]
 [0.2052899  0.7947101 ]
 [0.9477671  0.05223293]
 [0.2274592  0.7725408 ]
 [0.553921   0.446079  ]
 [0.5214083  0.47859162]
...
 [0.8718534  0.1281466 ]
 [0.03219423 0.96780574]
 [0.2274592  0.7725408 ]]
```

If you get `accuracy: 0.9275` at the end of the 30 epochs of training,
or better said, if you get an accuracy of >0.9 (90%) then the training seems
to have been successful.

Seeing the output later consisting of values like
```bash
[[0.9476151  0.05238493] # 94% no side-effect
 [0.2052899  0.7947101 ] # 79% with side-effect
 [0.9477671  0.05223293] # 94% no  side-effect (5% with side-effect) 
```
shows that the test data was able to determine  a  high  accuracy,
when running it  with new random values. So the model seems to have been trained
successfully and gives results with high accuracy for "unknown" test data.

If you run it in the jupyter notebook, you even get a confusion matrix,
which  is showing the data in a chart, that is easier to understand.
See the last section of the code example  in the file [1-patients-example.py](./1-patients-example1.py) for that.