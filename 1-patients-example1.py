#!/usr/bin/env python
# coding: utf-8

# This is the code what she teaches in the first hour in https://www.youtube.com/watch?v=qFJeN9V1ZsI

import numpy as np
from random import randint
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler

train_labels = []
train_samples = []
for i in range(50):
    train_samples.append(randint(13, 64))
    train_labels.append(1)    
    train_samples.append(randint(65, 100))
    train_labels.append(0)
for i in range(1000):
    train_samples.append(randint(13, 64))
    train_labels.append(0)
    train_samples.append(randint(65, 100))
    train_labels.append(1)
train_labels = np.array(train_labels)
train_samples = np.array(train_samples)
train_labels, train_samples = shuffle(train_labels, train_samples)

scaler = MinMaxScaler(feature_range=(0,1))
# `reshape` the data from 1D to fit in the fit() function ... instead  of x we need [x]
# I  assume because the array could contain any number of el, but we only have one, so it looks odd
scaled_train_samples = scaler.fit_transform(train_samples.reshape(-1, 1))
#print(scaled_train_samples)

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation,  Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy

gpus = tf.config.experimental.list_physical_devices('GPU')
print('GPUs = ', len(gpus))
#tf.config.experimental.set_memory_growth(gpus[0], True)

model = Sequential([
    Dense(units=16, input_shape=(1,), activation='relu'),
    Dense(units=32, activation='relu'),
    Dense(units=2, activation='softmax'),
])
#model.summary()
model.compile(optimizer=Adam(learning_rate=0.0001), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
#model.fit(x=scaled_train_samples, y=train_labels, batch_size=10,  epochs=30, shuffle=True, verbose=2)
model.fit(x=scaled_train_samples, y=train_labels, validation_split=0.1, batch_size=10,  epochs=30, shuffle=True, verbose=2)

test_labels = []
test_samples = []
for i in range(10):
    test_samples.append(randint(13, 64))
    test_labels.append(1)    
    test_samples.append(randint(65, 100))
    test_labels.append(0)
    
for i in range(200):
    test_samples.append(randint(13, 64))
    test_labels.append(0)
    test_samples.append(randint(65, 100))
    test_labels.append(1)
test_labels = np.array(test_labels)
test_samples = np.array(test_samples)
test_labels, test_samples = shuffle(test_labels, test_samples)
scaled_test_samples = scaler.fit_transform(test_samples.reshape(-1, 1))

# # Predict
predictions = model.predict(x=scaled_test_samples, batch_size=10, verbose=0)
print(predictions)

# seems to work ONLY in  jupyter
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix
rounded_predictions = np.argmax(predictions, axis=-1)
cm = confusion_matrix(y_true=test_labels, y_pred=rounded_predictions)
cm_plot_labels = ['no_side_fx', 'has_side_fx']
disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=cm_plot_labels)
disp.plot()
