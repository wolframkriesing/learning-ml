#!/usr/bin/env python
# coding: utf-8

# Learned that even/odd is not soo easy, so I changed the input 
# to be 2D, did also fail.
# Thomas says a LSTM should work for it.

import numpy as np
from random import randint
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler(feature_range=(0,1))
def reshape_and_transform_to_fit(sample):
  # `reshape` the data from 1D to fit in the fit() function ... instead  of x we need [x]
  # I  assume because the array could contain any number of el, but we only have one, so it looks odd
  return scaler.fit_transform(train_samples.reshape(-1, 2))

IS_EVEN = 0
IS_ODD = 1
train_labels = []
train_samples = []
for i in range(1000):
  number = randint(0, 10000000)
  train_samples.append([number, number])
  if (number % 2) == 0:
    train_labels.append(IS_EVEN)
    #print('even', number)
  else:
    train_labels.append(IS_ODD)
    #print('odd ', number)
    
train_labels = np.array(train_labels)
train_samples = np.array(train_samples)
train_labels, train_samples = shuffle(train_labels, train_samples)

print('some numbers (samples):', train_samples[0:10], '...')
print('their labels (0=EVEN, 1=ODD):', train_labels[0:10], '...')
print('amount of even numbers: ', len([i for i in train_labels if i == IS_EVEN])) 
print('amount of odd numbers: ', len([i for i in train_labels if i == IS_ODD])) 
    

scaled_train_samples = reshape_and_transform_to_fit(train_samples)
# print(scaled_train_samples)
print('some transformed and fit (SCALED) numbers (samples):', scaled_train_samples[0:10], '...')

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation,  Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy

gpus = tf.config.experimental.list_physical_devices('GPU')
print('number of GPUs = ', len(gpus))
#tf.config.experimental.set_memory_growth(gpus[0], True)

model = Sequential([
    Dense(units=16, input_shape=(2,), activation='relu'),
    Dense(units=32, activation='relu'),
    Dense(units=2, activation='softmax'),
])
#model.summary()
model.compile(optimizer=Adam(learning_rate=0.0001), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
#model.fit(x=scaled_train_samples, y=train_labels, batch_size=10,  epochs=30, shuffle=True, verbose=2)
model.fit(x=scaled_train_samples, y=train_labels, validation_split=0.1, batch_size=10,  epochs=30, shuffle=True, verbose=2)

test_labels = []
test_samples = []
for i in range(100):
  number = randint(0, 10000000)
  test_samples.append([number, number])
  if (number % 2) == 0:
    test_labels.append(IS_EVEN)
  else:
    test_labels.append(IS_ODD)
    
test_labels = np.array(test_labels)
test_samples = np.array(test_samples)
test_labels, test_samples = shuffle(test_labels, test_samples)
scaled_test_samples = reshape_and_transform_to_fit(test_samples)

# # Predict
predictions = model.predict(x=scaled_test_samples, batch_size=10, verbose=0)
print(predictions)
