#!/usr/bin/env bash

set -o errexit  # fail on simple (non-piped) error
set -o pipefail # also fail on piped commands (e.g. cat myfile.txt | grep timo)
set -o nounset  # fail when accessing unset vars

DOCKERFILE_HASH=$(md5 -q ./Dockerfile)
CONTAINER_NAME=jupyter
IMAGE_NAME=${CONTAINER_NAME}:${DOCKERFILE_HASH}

function is_container_running() {
  if [[ -n $(docker container ls --quiet --filter "name=$CONTAINER_NAME") ]]; then
    return 0
  fi;
  return 1
}

if [[ $(docker inspect --format . "${IMAGE_NAME}" 2>&1) != "." ]]; then
  echo "--- BUILDING image '${IMAGE_NAME}'---"
  docker image build -t "${IMAGE_NAME}" - < Dockerfile
fi

if is_container_running; then
  echo "--- ENTER running container '${CONTAINER_NAME}'---"
  docker container exec -it ${CONTAINER_NAME} "$@"
else
  echo "--- RUNNING container '${CONTAINER_NAME}'---"
  docker container run --rm -it \
    --name ${CONTAINER_NAME} \
    --volume "$(pwd)":/app \
    --publish 8888:8888 \
    "${IMAGE_NAME}" "$@"
fi
